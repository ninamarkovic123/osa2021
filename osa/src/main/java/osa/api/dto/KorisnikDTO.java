package osa.api.dto;

import java.io.Serializable;

import osa.api.model.Korisnik;

public class KorisnikDTO implements Serializable{
	private Integer id;
	
	private String ime;
	
	private String prezime;
	
	private String korisnickoime;
	
	private String lozinka;
	
    private String nova_lozinka;
	
	private Boolean blocked;
	
	private String tip_korisnika;

	public KorisnikDTO() {
		super();
	}

	public KorisnikDTO(String ime, String prezime, String korisnickoime, String lozinka, Boolean blocked,
			String tip_korisnika) {
		super();
		this.ime = ime;
		this.prezime = prezime;
		this.korisnickoime = korisnickoime;
		this.lozinka = lozinka;
		this.blocked = blocked;
		this.tip_korisnika = tip_korisnika;
	}

	public KorisnikDTO(Integer id, String ime, String prezime, String korisnickoime, String lozinka, Boolean blocked,
			String tip_korisnika) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.korisnickoime = korisnickoime;
		this.lozinka = lozinka;
		this.blocked = blocked;
		this.tip_korisnika = tip_korisnika;
	}

	public KorisnikDTO(Integer id, String ime, String prezime, String korisnickoime, String lozinka,
			String nova_lozinka, Boolean blocked, String tip_korisnika) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.korisnickoime = korisnickoime;
		this.lozinka = lozinka;
		this.nova_lozinka = nova_lozinka;
		this.blocked = blocked;
		this.tip_korisnika = tip_korisnika;
	}
	public KorisnikDTO(Korisnik k) {
		this(k.getId(), k.getIme(), k.getPrezime(), k.getKorisnickoime(), k.getLozinka(), k.getBlocked(), k.getUloga().name());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getKorisnickoime() {
		return korisnickoime;
	}

	public void setKorisnickoime(String korisnickoime) {
		this.korisnickoime = korisnickoime;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public String getNova_lozinka() {
		return nova_lozinka;
	}

	public void setNova_lozinka(String nova_lozinka) {
		this.nova_lozinka = nova_lozinka;
	}

	public Boolean getBlocked() {
		return blocked;
	}

	public void setBlocked(Boolean blocked) {
		this.blocked = blocked;
	}

	public String getTip_korisnika() {
		return tip_korisnika;
	}

	public void setTip_korisnika(String tip_korisnika) {
		this.tip_korisnika = tip_korisnika;
	}
	
	
}
