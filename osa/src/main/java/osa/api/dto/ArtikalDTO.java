package osa.api.dto;

import java.io.Serializable;

import osa.api.model.Artikal;

public class ArtikalDTO implements Serializable {
	private Integer id;
	
	private String naziv;
		
	private String opis;

	private Double cena;
	
	private String path;
	
    private byte[] imgBytes;
	
	private ProdavacDTO prodavacDTO;
	
	private String korisnickoimeProdavca;

	public ArtikalDTO() {
		super();
	}
	
	public ArtikalDTO(Artikal a) {
		this(a.getId(), a.getNaziv(), a.getOpis(), a.getCena(), a.getPath(), new ProdavacDTO(a.getProdavac()));
	}
	
	public ArtikalDTO(String naziv, String opis, Double cena, byte[] imgBytes, String korisnickoimeProdavca) {
		super();
		this.naziv = naziv;
		this.opis = opis;
		this.cena = cena;
		this.imgBytes = imgBytes;
		this.korisnickoimeProdavca = korisnickoimeProdavca;
	}

	public ArtikalDTO(Integer id, String naziv, String opis, Double cena, String path, ProdavacDTO prodavacDTO) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.opis = opis;
		this.cena = cena;
		this.path = path;
		this.prodavacDTO = prodavacDTO;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public byte[] getImgBytes() {
		return imgBytes;
	}

	public void setImgBytes(byte[] imgBytes) {
		this.imgBytes = imgBytes;
	}

	public ProdavacDTO getProdavacDTO() {
		return prodavacDTO;
	}

	public void setProdavacDTO(ProdavacDTO prodavacDTO) {
		this.prodavacDTO = prodavacDTO;
	}

	public String getKorisnickoimeProdavca() {
		return korisnickoimeProdavca;
	}

	public void setKorisnickoimeProdavca(String korisnickoimeProdavca) {
		this.korisnickoimeProdavca = korisnickoimeProdavca;
	}
	
	
}
