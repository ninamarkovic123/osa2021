package osa.api.dto;

import java.io.Serializable;

import osa.api.model.StavkaPorudzbine;

public class StavkaPorudzbineDTO implements Serializable {

	
	private Integer id;
	
	private Integer kolicina;
	
	private ArtikalDTO artikal;
	
	private PorudzbinaDTO porudzbina;

	public StavkaPorudzbineDTO() {
		super();
	}

	public StavkaPorudzbineDTO(Integer kolicina, ArtikalDTO artikal, PorudzbinaDTO porudzbina) {
		super();
		this.kolicina = kolicina;
		this.artikal = artikal;
		this.porudzbina = porudzbina;
	}

	public StavkaPorudzbineDTO(Integer id, Integer kolicina, ArtikalDTO artikal, PorudzbinaDTO porudzbina) {
		super();
		this.id = id;
		this.kolicina = kolicina;
		this.artikal = artikal;
		this.porudzbina = porudzbina;
	}
	public StavkaPorudzbineDTO(StavkaPorudzbine s) {
		this(s.getId(), s.getKolicina(), new ArtikalDTO(s.getArtikal()), new PorudzbinaDTO(s.getPorudzbina()));
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getKolicina() {
		return kolicina;
	}

	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}

	public ArtikalDTO getArtikal() {
		return artikal;
	}

	public void setArtikal(ArtikalDTO artikal) {
		this.artikal = artikal;
	}

	public PorudzbinaDTO getPorudzbina() {
		return porudzbina;
	}

	public void setPorudzbina(PorudzbinaDTO porudzbina) {
		this.porudzbina = porudzbina;
	}
	

}
