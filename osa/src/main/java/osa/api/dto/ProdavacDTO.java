package osa.api.dto;

import osa.api.model.Prodavac;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ProdavacDTO implements Serializable{

	private Integer id;
	
	private String ime;
	
	private String prezime;
	
	private String korisnickoime;
	
	private String lozinka;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date radiOd;
	
	private String email;

	private String adresa;
	
	private String naziv;

	public ProdavacDTO() {
		super();
	}

	public ProdavacDTO(String ime, String prezime, String korisnickoime, String lozinka, Date radiOd, String email,
			String adresa, String naziv) {
		super();
		this.ime = ime;
		this.prezime = prezime;
		this.korisnickoime = korisnickoime;
		this.lozinka = lozinka;
		this.radiOd = radiOd;
		this.email = email;
		this.adresa = adresa;
		this.naziv = naziv;
	}

	public ProdavacDTO(Integer id, Date radiOd, String email, String adresa, String naziv) {
		super();
		this.id = id;
		this.radiOd = radiOd;
		this.email = email;
		this.adresa = adresa;
		this.naziv = naziv;
	}
	public ProdavacDTO(Prodavac p) {
		this(p.getId(),p.getRadiOd(), p.getEmail(), p.getAdresa(),p.getNaziv());
		
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getKorisnickoime() {
		return korisnickoime;
	}

	public void setKorisnickoime(String korisnickoime) {
		this.korisnickoime = korisnickoime;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public Date getRadiOd() {
		return radiOd;
	}

	public void setRadiOd(Date radiOd) {
		this.radiOd = radiOd;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	


}
