package osa.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import osa.api.model.Popust;

public class PopustDTO implements Serializable{
	
	private Integer id;
	
	private Integer procenat;
		
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date datumOd;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date datumDo;
	
	private String text;
	
	private ProdavacDTO prodavac;
	
	private List<ArtikalDTO> artikli;

	public PopustDTO() {
		super();
	}

	
	public PopustDTO(Integer procenat, Date datumOd, Date datumDo, String text, ProdavacDTO prodavac) {
		super();
		this.procenat = procenat;
		this.datumOd = datumOd;
		this.datumDo = datumDo;
		this.text = text;
		this.prodavac = prodavac;
	}


	public PopustDTO(Integer procenat, Date datumOd, Date datumDo, String text, List<ArtikalDTO> artikli) {
		super();
		this.procenat = procenat;
		this.datumOd = datumOd;
		this.datumDo = datumDo;
		this.text = text;
		this.artikli = artikli;
	}


	public PopustDTO(Integer id, Integer procenat, Date datumOd, Date datumDo, String text, ProdavacDTO prodavac) {
		super();
		this.id = id;
		this.procenat = procenat;
		this.datumOd = datumOd;
		this.datumDo = datumDo;
		this.text = text;
		this.prodavac = prodavac;
	}

	public PopustDTO(Popust d) {
		this(d.getId(),d.getProcenat(), d.getDatumOd(), d.getDatumDo(), d.getText(), new ProdavacDTO(d.getProdavac()));
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProcenat() {
		return procenat;
	}

	public void setProcenat(Integer procenat) {
		this.procenat = procenat;
	}

	public Date getDatumOd() {
		return datumOd;
	}

	public void setDatumOd(Date datumOd) {
		this.datumOd = datumOd;
	}

	public Date getDatumDo() {
		return datumDo;
	}

	public void setDatumDo(Date datumDo) {
		this.datumDo = datumDo;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public ProdavacDTO getProdavac() {
		return prodavac;
	}

	public void setProdavac(ProdavacDTO prodavac) {
		this.prodavac = prodavac;
	}

	public List<ArtikalDTO> getArtikli() {
		return artikli;
	}

	public void setArtikli(List<ArtikalDTO> artikli) {
		this.artikli = artikli;
	}
	
	
}
