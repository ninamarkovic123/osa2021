package osa.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import osa.api.model.Porudzbina;

public class PorudzbinaDTO implements Serializable {
	
	private Integer id;
	
	private Date datumPorudzbine;
	
	private Boolean dostavljeno;
	
	private Integer ocena;

	private String komentar;

	private Boolean anonimanKomentar;

	private Boolean arhiviranKomentar;

	private KupacDTO kupac;
	
	private String kupac_korisnickoime;
	
	private List<StavkaPorudzbineDTO> stavka_porudzbine;

	public PorudzbinaDTO() {
		super();
	}

	public PorudzbinaDTO(Date datumPorudzbine, Boolean dostavljeno, Integer ocena, String komentar,
			Boolean anonimanKomentar, Boolean arhiviranKomentar, KupacDTO kupac) {
		super();
		this.datumPorudzbine = datumPorudzbine;
		this.dostavljeno = dostavljeno;
		this.ocena = ocena;
		this.komentar = komentar;
		this.anonimanKomentar = anonimanKomentar;
		this.arhiviranKomentar = arhiviranKomentar;
		this.kupac = kupac;
	}

	public PorudzbinaDTO(Integer id, Date datumPorudzbine, Boolean dostavljeno, Integer ocena, String komentar,
			Boolean anonimanKomentar, Boolean arhiviranKomentar, KupacDTO kupac) {
		super();
		this.id = id;
		this.datumPorudzbine = datumPorudzbine;
		this.dostavljeno = dostavljeno;
		this.ocena = ocena;
		this.komentar = komentar;
		this.anonimanKomentar = anonimanKomentar;
		this.arhiviranKomentar = arhiviranKomentar;
		this.kupac = kupac;
	}

	public PorudzbinaDTO(Integer id, Date datumPorudzbine, Boolean dostavljeno, Integer ocena, String komentar,
			Boolean anonimanKomentar, Boolean arhiviranKomentar, KupacDTO kupac,
			List<StavkaPorudzbineDTO> stavka_porudzbine) {
		super();
		this.id = id;
		this.datumPorudzbine = datumPorudzbine;
		this.dostavljeno = dostavljeno;
		this.ocena = ocena;
		this.komentar = komentar;
		this.anonimanKomentar = anonimanKomentar;
		this.arhiviranKomentar = arhiviranKomentar;
		this.kupac = kupac;
		this.stavka_porudzbine = stavka_porudzbine;
	}
	public PorudzbinaDTO(Porudzbina o) {
		this(o.getId(), o.getPorudzbinaDatum(), o.getDostavljeno(), o.getOcena(), o.getKomentar(), 
				o.getAnonimanKomentar(), o.getArhiviranKomentar(), new KupacDTO(o.getKupac()));
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDatumPorudzbine() {
		return datumPorudzbine;
	}

	public void setDatumPorudzbine(Date datumPorudzbine) {
		this.datumPorudzbine = datumPorudzbine;
	}

	public Boolean getDostavljeno() {
		return dostavljeno;
	}

	public void setDostavljeno(Boolean dostavljeno) {
		this.dostavljeno = dostavljeno;
	}

	public Integer getOcena() {
		return ocena;
	}

	public void setOcena(Integer ocena) {
		this.ocena = ocena;
	}

	public String getKomentar() {
		return komentar;
	}

	public void setKomentar(String komentar) {
		this.komentar = komentar;
	}

	public Boolean getAnonimanKomentar() {
		return anonimanKomentar;
	}

	public void setAnonimanKomentar(Boolean anonimanKomentar) {
		this.anonimanKomentar = anonimanKomentar;
	}

	public Boolean getArhiviranKomentar() {
		return arhiviranKomentar;
	}

	public void setArhiviranKomentar(Boolean arhiviranKomentar) {
		this.arhiviranKomentar = arhiviranKomentar;
	}

	public KupacDTO getKupac() {
		return kupac;
	}

	public void setKupac(KupacDTO kupac) {
		this.kupac = kupac;
	}

	public String getKupac_korisnickoime() {
		return kupac_korisnickoime;
	}

	public void setKupac_korisnickoime(String kupac_korisnickoime) {
		this.kupac_korisnickoime = kupac_korisnickoime;
	}

	public List<StavkaPorudzbineDTO> getStavka_porudzbine() {
		return stavka_porudzbine;
	}

	public void setStavka_porudzbine(List<StavkaPorudzbineDTO> stavka_porudzbine) {
		this.stavka_porudzbine = stavka_porudzbine;
	}
	
	

}
