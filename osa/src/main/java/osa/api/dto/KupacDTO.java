package osa.api.dto;

import java.io.Serializable;

import osa.api.model.Kupac;

public class KupacDTO implements Serializable {
	private Integer id;
	
	private String korisnickoime;
	
	private String lozinka;
	
	private String adresa;
	
	private String ime;
	
	private String prezime;

	public KupacDTO() {
		super();
	}
	

	public KupacDTO(Integer id, String adresa) {
		super();
		this.id = id;
		this.adresa = adresa;
	}


	public KupacDTO(Integer id, String korisnickoime, String lozinka, String adresa, String ime, String prezime) {
		super();
		this.id = id;
		this.korisnickoime = korisnickoime;
		this.lozinka = lozinka;
		this.adresa = adresa;
		this.ime = ime;
		this.prezime = prezime;
	}

	public KupacDTO(Kupac k) {
		this(k.getId(), k.getAdresa());
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getKorisnickoime() {
		return korisnickoime;
	}

	public void setKorisnickoime(String korisnickoime) {
		this.korisnickoime = korisnickoime;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	
	
}
