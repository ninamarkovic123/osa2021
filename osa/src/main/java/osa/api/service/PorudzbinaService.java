package osa.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import osa.api.model.Kupac;
import osa.api.model.Porudzbina;
import osa.api.repository.PorudzbinaRepository;

@Service
public class PorudzbinaService implements PorudzbinaServiceInterface {

	@Autowired
	private PorudzbinaRepository repository;
	
	@Override
	public List<Porudzbina> findAllByAnonimanKomentar(Boolean anoniman) {
		// TODO Auto-generated method stub
		return repository.findAllByAnonimanKomentar(anoniman);
	}

	@Override
	public List<Porudzbina> findAllByArhiviranKomentar(Boolean arhiviran) {
		// TODO Auto-generated method stub
		return repository.findAllByArhiviranKomentar(arhiviran);
	}

	@Override
	public List<Porudzbina> findByDostavljeno(Boolean dostavljeno) {
		// TODO Auto-generated method stub
		return repository.findByDostavljeno(dostavljeno);
	}

	@Override
	public List<Porudzbina> findByKupac(Kupac c) {
		// TODO Auto-generated method stub
		return repository.findByKupac(c);
	}

	@Override
	public List<Porudzbina> findByKupacAndDostavljeno(Kupac c, Boolean dostavljeno) {
		// TODO Auto-generated method stub
		return repository.findByKupacAndDostavljeno(c, dostavljeno);
	}

	@Override
	public Porudzbina findByKomentar(String komentar) {
		// TODO Auto-generated method stub
		return repository.findByKomentar(komentar);
	}

	@Override
	public List<Porudzbina> findByOcena(Integer ocena) {
		// TODO Auto-generated method stub
		return repository.findByOcena(ocena);
	}

	@Override
	public List<Porudzbina> findByKomentarNotNullAndArhiviranKomentar(Boolean anoniman) {
		// TODO Auto-generated method stub
		return repository.findByKomentarNotNullAndArhiviranKomentar(anoniman);
	}

	@Override
	public Porudzbina getOne(Integer id) {
		// TODO Auto-generated method stub
		return repository.getOne(id);
	}

	@Override
	public Porudzbina save(Porudzbina porudzbina) {
		// TODO Auto-generated method stub
		return repository.save(porudzbina);
	}

	@Override
	public void remove(Integer id) {
		repository.deleteById(id);
	}

	@Override
	public List<Porudzbina> findAll() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

}
