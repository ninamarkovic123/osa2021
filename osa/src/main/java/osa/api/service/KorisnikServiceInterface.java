package osa.api.service;

import java.util.List;

import osa.api.dto.KorisnikDTO;
import osa.api.dto.KupacDTO;
import osa.api.dto.ProdavacDTO;
import osa.api.model.Korisnik;
import osa.api.model.Kupac;
import osa.api.model.Prodavac;

public interface KorisnikServiceInterface {
	List<Korisnik> findAllByBlocked(Boolean blocked);
	
	Korisnik findByKorisnickoime(String korisnickoime);

	Korisnik findByIme(String ime);

	Korisnik findByPrezime(String prezime);
	
	Korisnik findByKorisnickoimeAndLozinka(String korisnickoime, String lozinka);
	
	Korisnik findByKorisnickoimeAndBlocked(String korisnickoime, Boolean blocked);
	
	Korisnik getOne(Integer id);
	
	void save(KorisnikDTO korisnikDTO);
	
	void saveKupac(KupacDTO kupac);
	
	void saveProdavac(ProdavacDTO prodavac);
	
	Boolean saveKorisnik(Korisnik u);
	
	Boolean editKorisnik(Korisnik u);

	Boolean changePassword(Korisnik u);

	void remove(Integer id);
	
	List<Korisnik> findAll();

	
	List<Prodavac> getAllProdavci();
	
	Kupac getKupac(Integer id);
	
	Prodavac getProdavac(Integer id);

}
