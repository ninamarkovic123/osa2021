package osa.api.service;

import java.util.List;

import osa.api.model.Popust;
import osa.api.model.Prodavac;

public interface PopustServiceInterface {
	List<Popust> findAll();
	
	Popust findOne(Integer id);
	
	Popust save(Popust popust);
	
	void remove(Integer id);
	
	List<Popust> findAllByProcenat(Integer per);
	
	Popust findByText(String text);

	Popust findByProdavac_Id(Integer id);
	
	List<Popust>  findByProdavac(Prodavac p);

}
