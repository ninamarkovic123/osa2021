package osa.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import osa.api.model.StavkaPorudzbine;
import osa.api.repository.StavkaPorudzbineRepository;

@Service
public class StavkaPorudzbineService implements StavkaPorudzbinaInterface {

	@Autowired
	StavkaPorudzbineRepository repo;
	
	@Override
	public List<StavkaPorudzbine> findAll() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public StavkaPorudzbine save(StavkaPorudzbine stavka) {
		// TODO Auto-generated method stub
		return repo.save(stavka);
	}

	@Override
	public List<StavkaPorudzbine> findAllByKolicina(Integer kolicina) {
		// TODO Auto-generated method stub
		return repo.findAllByKolicina(kolicina);
	}

	@Override
	public StavkaPorudzbine findByKolicina(Integer kolicina) {
		// TODO Auto-generated method stub
		return repo.findByKolicina(kolicina);
	}

	@Override
	public StavkaPorudzbine findByPorudzbina_Id(Integer id) {
		// TODO Auto-generated method stub
		return repo.findByPorudzbina_Id(id);
	}

	@Override
	public StavkaPorudzbine findByArtikal_Id(Integer id) {
		// TODO Auto-generated method stub
		return repo.findByArtikal_Id(id);
	}

	@Override
	public StavkaPorudzbine findOne(Integer id) {
		// TODO Auto-generated method stub
		return repo.getOne(id);
	}

	@Override
	public void remove(Integer id) {
		repo.deleteById(id);
	}

}
