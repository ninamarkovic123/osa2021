package osa.api.service;

import java.util.List;

import osa.api.model.StavkaPorudzbine;

public interface StavkaPorudzbinaInterface {
	
	List<StavkaPorudzbine> findAll();
	
	StavkaPorudzbine save(StavkaPorudzbine stavka);	

	List<StavkaPorudzbine> findAllByKolicina(Integer kolicina);
	
	StavkaPorudzbine findByKolicina(Integer kolicina);

	StavkaPorudzbine findByPorudzbina_Id(Integer id);
	
	StavkaPorudzbine findByArtikal_Id(Integer id);

	StavkaPorudzbine findOne(Integer id);

	void remove(Integer id);
	
}
