package osa.api.service;

import java.util.Date;
import java.util.List;

import osa.api.model.Artikal;

public interface ArtikalServiceInterface {
	Artikal findByCena(Double cena);
	
	
	Artikal findByNaziv(String naziv);
	
	Artikal save(Artikal artikal);
	
	Artikal findOne(Integer id);
	
	void remove(Integer id);
	
	List<Artikal> findAll();
	
	List<Artikal> findAllByProdavac(String naziv);

	List<Artikal> findAllByProdvac_Id(Integer id);
	
	Artikal findByProdavac_Id(Integer prodavac_id);
	
	List<Artikal> findAllByProdavacAndPopust(Integer prodavac_id, Date new_date_from, Date new_date_until);
}
