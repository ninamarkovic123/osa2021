package osa.api.service;

import java.util.List;

import osa.api.model.Kupac;
import osa.api.model.Porudzbina;

public interface PorudzbinaServiceInterface {

	List<Porudzbina> findAllByAnonimanKomentar(Boolean anoniman);
	
	List<Porudzbina> findAllByArhiviranKomentar(Boolean arhiviran);
	
	List<Porudzbina> findByDostavljeno(Boolean dostavljeno);
	
	List<Porudzbina> findByKupac(Kupac c);
	
	List<Porudzbina> findByKupacAndDostavljeno(Kupac c, Boolean dostavljeno);

	Porudzbina findByKomentar(String komentar);

	List<Porudzbina> findByOcena(Integer ocena);
	
	List<Porudzbina> findByKomentarNotNullAndArhiviranKomentar(Boolean anoniman);
	
	Porudzbina getOne(Integer id);
	
	Porudzbina save(Porudzbina porudzbina);
	
	void remove(Integer id);
	
	List<Porudzbina> findAll();
}
