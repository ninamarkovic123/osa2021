package osa.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import osa.api.model.Popust;
import osa.api.model.Prodavac;
import osa.api.repository.PopustRepository;

@Service
public class PopustService implements PopustServiceInterface {

	@Autowired
	PopustRepository repos;
	
	@Override
	public List<Popust> findAll() {
		// TODO Auto-generated method stub
		return repos.findAll();
	}

	@Override
	public Popust findOne(Integer id) {
		// TODO Auto-generated method stub
		return repos.getOne(id);
	}

	@Override
	public Popust save(Popust popust) {
		// TODO Auto-generated method stub
		return repos.save(popust);
	}

	@Override
	public void remove(Integer id) {
		repos.deleteById(id);
	}

	@Override
	public List<Popust> findAllByProcenat(Integer per) {
		// TODO Auto-generated method stub
		return repos.findAllByProcenat(per);
	}

	@Override
	public Popust findByText(String text) {
		// TODO Auto-generated method stub
		return repos.findByText(text);
	}

	@Override
	public Popust findByProdavac_Id(Integer id) {
		// TODO Auto-generated method stub
		return repos.findByProdavac_Id(id);
	}

	@Override
	public List<Popust> findByProdavac(Prodavac p) {
		// TODO Auto-generated method stub
		return repos.findByProdavac(p);
	}

}
