package osa.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import osa.api.dto.KorisnikDTO;
import osa.api.dto.KupacDTO;
import osa.api.dto.ProdavacDTO;
import osa.api.model.Korisnik;
import osa.api.model.Kupac;
import osa.api.model.Prodavac;
import osa.api.repository.KorisnikRepository;
import osa.api.repository.KupacRepository;
import osa.api.repository.ProdavacRepository;

@Service
public class KorisnikService implements KorisnikServiceInterface{

	@Autowired
	private KorisnikRepository repo;
	
	@Autowired
	private ProdavacRepository repop;
	
	@Autowired
	KupacRepository repok;
	
	@Override
	public List<Korisnik> findAllByBlocked(Boolean blocked) {
		// TODO Auto-generated method stub
		return repo.findAllByBlocked(blocked);
	}

	@Override
	public Korisnik findByKorisnickoime(String korisnickoime) {
		// TODO Auto-generated method stub
		return repo.findByKorisnickoime(korisnickoime);
	}

	@Override
	public Korisnik findByIme(String ime) {
		// TODO Auto-generated method stub
		return repo.findByIme(ime);
	}

	@Override
	public Korisnik findByPrezime(String prezime) {
		// TODO Auto-generated method stub
		return repo.findByPrezime(prezime);
	}

	@Override
	public Korisnik findByKorisnickoimeAndLozinka(String korisnickoime, String lozinka) {
		// TODO Auto-generated method stub
		return repo.findByKorisnickoimeAndLozinka(korisnickoime, lozinka);
	}

	@Override
	public Korisnik findByKorisnickoimeAndBlocked(String korisnickoime, Boolean blocked) {
		// TODO Auto-generated method stub
		return repo.findByKorisnickoimeAndBlocked(korisnickoime, blocked);
	}

	@Override
	public Korisnik getOne(Integer id) {
		// TODO Auto-generated method stub
		return repo.getOne(id);
	}

	@Override
	public void save(KorisnikDTO korisnikDTO) {
		
	}

	@Override
	public void saveKupac(KupacDTO kupac) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveProdavac(ProdavacDTO prodavac) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Boolean saveKorisnik(Korisnik u) {
		repo.save(u);
		return true;
	}

	@Override
	public Boolean editKorisnik(Korisnik u) {
		repo.save(u);
		return true;
	}

	@Override
	public Boolean changePassword(Korisnik u) {
		repo.save(u);
		return true;
	}

	@Override
	public void remove(Integer id) {
		repo.deleteById(id);
	}

	@Override
	public List<Korisnik> findAll() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public List<Prodavac> getAllProdavci() {
		// TODO Auto-generated method stub
		return repop.findAll();
	}

	@Override
	public Kupac getKupac(Integer id) {
		// TODO Auto-generated method stub
		return repok.getOne(id);
	}

	@Override
	public Prodavac getProdavac(Integer id) {
		// TODO Auto-generated method stub
		return repop.getOne(id);
	}

}
