package osa.api.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import osa.api.model.Artikal;
import osa.api.repository.ArtikalRepository;

@Service
public class ArtikalService implements ArtikalServiceInterface {

	@Autowired
	ArtikalRepository repo;
	
	@Override
	public Artikal findByCena(Double cena) {
		return repo.findByCena(cena);
	}

	@Override
	public Artikal findByNaziv(String naziv) {
		// TODO Auto-generated method stub
		return repo.findByNaziv(naziv);
	}

	@Override
	public Artikal save(Artikal artikal) {
		// TODO Auto-generated method stub
		return repo.save(artikal);
	}

	@Override
	public Artikal findOne(Integer id) {
		// TODO Auto-generated method stub
		return repo.getOne(id);
	}

	@Override
	public void remove(Integer id) {
		repo.deleteById(id);
		
	}

	@Override
	public List<Artikal> findAll() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public List<Artikal> findAllByProdavac(String naziv) {
		// TODO Auto-generated method stub
		return repo.findAllByProdavac_Naziv(naziv);
	}

	@Override
	public List<Artikal> findAllByProdvac_Id(Integer id) {
		// TODO Auto-generated method stub
		return repo.findAllByProdavac_Id(id);
	}

	@Override
	public Artikal findByProdavac_Id(Integer prodavac_id) {
		// TODO Auto-generated method stub
		return repo.findByProdavac_Id(prodavac_id);
	}

	@Override
	public List<Artikal> findAllByProdavacAndPopust(Integer prodavac_id, Date new_date_from, Date new_date_until) {
		// TODO Auto-generated method stub
		return repo.findAllByProdavacAndPopust(prodavac_id, new_date_from, new_date_until);
	}

}
