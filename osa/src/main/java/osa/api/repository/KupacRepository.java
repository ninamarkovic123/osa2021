package osa.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import osa.api.model.Kupac;

public interface KupacRepository extends JpaRepository<Kupac, Integer> {
	Kupac findByKorisnickoime(String korisnickoime);
}
