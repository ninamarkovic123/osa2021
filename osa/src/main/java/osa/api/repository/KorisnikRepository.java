package osa.api.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import osa.api.model.Korisnik;

public interface KorisnikRepository extends JpaRepository<Korisnik, Integer> {

	List<Korisnik> findAllByBlocked(Boolean blocked);
	
	Korisnik findByKorisnickoime(String korisnickoime);

	Korisnik findByIme(String ime);

	Korisnik findByPrezime(String prezime);
	
	Korisnik findByKorisnickoimeAndLozinka(String korisnickoime, String lozinka);
	
	Korisnik findByKorisnickoimeAndBlocked(String korisnickoime, Boolean blocked);

}
