package osa.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import osa.api.model.Kupac;
import osa.api.model.Porudzbina;

public interface PorudzbinaRepository extends JpaRepository<Porudzbina, Integer> {

	List<Porudzbina> findAllByAnonimanKomentar(Boolean anoniman);
	
	List<Porudzbina> findAllByArhiviranKomentar(Boolean arhiviran);
	
	List<Porudzbina> findByDostavljeno(Boolean dostavljeno);
	
	List<Porudzbina> findByKupac(Kupac c);
	
	List<Porudzbina> findByKupacAndDostavljeno(Kupac c, Boolean dostavljeno);

	Porudzbina findByKomentar(String komentar);

	List<Porudzbina> findByOcena(Integer ocena);
	
	List<Porudzbina> findByKomentarNotNullAndArhiviranKomentar(Boolean anoniman);
}
