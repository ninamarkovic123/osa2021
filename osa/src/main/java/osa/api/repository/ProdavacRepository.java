package osa.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import osa.api.model.Prodavac;

public interface ProdavacRepository extends JpaRepository<Prodavac, Integer> {

	Prodavac findByKorisnickoime(String korisnickoime);
	
}
