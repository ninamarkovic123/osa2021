package osa.api.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import osa.api.model.StavkaPorudzbine;

public interface StavkaPorudzbineRepository extends JpaRepository<StavkaPorudzbine, Integer>{

	List<StavkaPorudzbine> findAllByKolicina(Integer kolicina);
	
	StavkaPorudzbine findByKolicina(Integer kolicina);

	StavkaPorudzbine findByPorudzbina_Id(Integer id);
	
	StavkaPorudzbine findByArtikal_Id(Integer id);
}
