package osa.api.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.FluentQuery.FetchableFluentQuery;
import org.springframework.data.repository.query.Param;

import osa.api.model.Artikal;



public interface ArtikalRepository extends JpaRepository<Artikal, Integer> {

	Artikal findByCena(Double cena);
	
	Artikal findByNaziv(String naziv);
	
	List<Artikal> findAllByProdavac_Naziv(String nazic);
	
	List<Artikal> findAllByProdavac_Id(Integer id);

	Artikal findByProdavac_Id(Integer prodavac_id);
	
    @Query(value="select artikli.artikal_id, artikli.naziv, artikli.cena, artikli.opis, artikli.path, artikli.prodavac_fk from artikli where (artikli.prodavac_fk = :prodavac_fk and artikli.artikal_id in (select artikal_id from artikal_popust where artikal_popust.popust_id in (select popust_id from popusti where popusti.datum_od NOT BETWEEN :new_date_from  and :new_date_until and popusti.date_do NOT BETWEEN :new_date_from and :new_date_until and :new_date_from NOT BETWEEN popusti.datum_od and popusti.datum_do and :new_date_until NOT BETWEEN popusti.datum_od and popusti.datum_do))) or (artikli.artikal_id not in (select artikal_id from artikal_popust))", nativeQuery = true)
	List<Artikal> findAllByProdavacAndPopust(@Param("prodavac_fk") Integer prodavac_fk,@Param("new_date_from")Date new_date_from,@Param("new_date_until") Date new_date_until);
	

}
