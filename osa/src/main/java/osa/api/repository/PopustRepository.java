package osa.api.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import osa.api.model.Popust;
import osa.api.model.Prodavac;
public interface PopustRepository extends JpaRepository<Popust, Integer> {
	
	List<Popust> findAllByProcenat(Integer procenat);
	
	Popust findByText(String text);

	Popust findByProdavac_Id(Integer id);
	
	List<Popust> findByProdavac(Prodavac s);
}
