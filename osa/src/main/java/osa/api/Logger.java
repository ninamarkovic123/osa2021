package osa.api;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class Logger {
	
	@Before(value = "execution(* osa.api.controller.*.*(..))")
	public void logBeforeAccess(JoinPoint joinPoint) {
		System.out.println("Started:"+ joinPoint);
	}
	
    @After(value = "execution(* osa.api.controller.*.*(..))")
	public void logAfterAccess(JoinPoint joinPoint) {
		System.out.println("Ended: " + joinPoint);
	}

}
