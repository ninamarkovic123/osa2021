package osa.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="stavka_porudzbine")
public class StavkaPorudzbine {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="stavka_porudzbine_id", unique=true, nullable=false)
	private Integer id;
	
	@Column(name="kolicina", unique=false, nullable=false)
	private Integer kolicina;
	
	@ManyToOne
	@JoinColumn(name="artikal_fk")
	private Artikal artikal;
	
	@ManyToOne
	@JoinColumn(name="porudzbina_fk")
	private Porudzbina porudzbina;
	
	public StavkaPorudzbine(){
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getKolicina() {
		return kolicina;
	}

	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}

	public Artikal getArtikal() {
		return artikal;
	}

	public void setArtikal(Artikal artikal) {
		this.artikal = artikal;
	}

	public Porudzbina getPorudzbina() {
		return porudzbina;
	}

	public void setPorudzbina(Porudzbina porudzbina) {
		this.porudzbina = porudzbina;
	}
	
}
