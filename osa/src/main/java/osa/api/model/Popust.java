package osa.api.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="popusti")
public class Popust {
	@Id
	@GeneratedValue(strategy=IDENTITY)
	@Column(name="popust_id", unique=true, nullable=false)
	private Integer id;
	
	@Column(name="procenat",unique=false,nullable=false)
	private Integer procenat;
		
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
	@Column(name="datum_od", unique=false, nullable=false)
	private Date datumOd;

	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
	@Column(name="datum_do", unique=false, nullable=false)
	private Date datumDo;
	
	@Column(name="text", unique=false, nullable=false)
	private String text;
	
	@ManyToOne
	@JoinColumn(name="prodavac_fk")
	private Prodavac prodavac;

	@ManyToMany(cascade = { CascadeType.ALL })@JoinTable(name = "artikal_popust", 
    joinColumns ={@JoinColumn(name ="popust_id")},inverseJoinColumns = { @JoinColumn(name = "artikal_id")})
    Set<Artikal> artikli = new HashSet<>();

	public Popust() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProcenat() {
		return procenat;
	}

	public void setProcenat(Integer procenat) {
		this.procenat = procenat;
	}

	public Date getDatumOd() {
		return datumOd;
	}

	public void setDatumOd(Date datumOd) {
		this.datumOd = datumOd;
	}

	public Date getDatumDo() {
		return datumDo;
	}

	public void setDatumDo(Date datumDo) {
		this.datumDo = datumDo;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Prodavac getProdavac() {
		return prodavac;
	}

	public void setProdavac(Prodavac prodavac) {
		this.prodavac = prodavac;
	}

	public Set<Artikal> getArtikli() {
		return artikli;
	}

	public void setArtikli(Set<Artikal> artikli) {
		this.artikli = artikli;
	}
	

}
