package osa.api.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="artikli")
public class Artikal {
	@Id
	@GeneratedValue(strategy=IDENTITY)
	@Column(name="artikal_id", unique=true, nullable=false)
	private Integer id;
	
	@Column(name="naziv",unique=false,nullable=false)
	private String naziv;
		
	@Column(name="opis", unique=false, nullable=false)
	private String opis;

	@Column(name="cena", unique=false, nullable=false)
	private Double cena;
	
	@Column(name="path", unique=false, nullable=false)
	private String path;
	
	@ManyToOne 
	@JoinColumn(name="prodavac_fk")
	private Prodavac prodavac;
	
	@ManyToMany(mappedBy="artikli")
    private Set<Popust> popust = new HashSet<>();
	
	@OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="artikal")
	private Set<StavkaPorudzbine> stavkaPorudzbine = new HashSet<StavkaPorudzbine>();
	
	public Artikal() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Prodavac getProdavac() {
		return prodavac;
	}

	public void setProdavac(Korisnik korisnik) {
		this.prodavac = (Prodavac) korisnik;
	}

	public Set<Popust> getPopust() {
		return popust;
	}

	public void setPopust(Set<Popust> popust) {
		this.popust = popust;
	}

	public Set<StavkaPorudzbine> getStavkaPorudzbine() {
		return stavkaPorudzbine;
	}

	public void setStavkaPorudzbine(Set<StavkaPorudzbine> stavkaPorudzbine) {
		this.stavkaPorudzbine = stavkaPorudzbine;
	}
	

}
