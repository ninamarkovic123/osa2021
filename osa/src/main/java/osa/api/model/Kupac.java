package osa.api.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue("Kupac")
public class Kupac extends Korisnik{
	
	@Column(name="adresa", unique=false, nullable=true)
	private String adresa;
	
	@OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="kupac")
	private Set<Porudzbina> porudzbina = new HashSet<Porudzbina>();

	public Kupac() {
		
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public Set<Porudzbina> getPorudzbina() {
		return porudzbina;
	}

	public void setPorudzbina(Set<Porudzbina> porudzbina) {
		this.porudzbina = porudzbina;
	}
	
	
}
