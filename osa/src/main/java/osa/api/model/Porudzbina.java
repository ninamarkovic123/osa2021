package osa.api.model;



import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="porudzbina")
public class Porudzbina implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="porudzbina_id", unique=true, nullable=false)
	private Integer id;
	
	@Column(name="porudzbina_datum", unique=false, nullable=false)
	private Date porudzbinaDatum;
	
	@Column(name="dostavljeno", unique=false, nullable=false)
	private Boolean dostavljeno;
	
	@Column(name="ocena", unique=false, nullable=true)
	private Integer ocena;

	@Column(name="komentar", unique=false, nullable=true)
	private String komentar;

	@Column(name="anoniman_komentar", unique=false, nullable=true)
	private Boolean anonimanKomentar;

	@Column(name="arhiviran_komentar", unique=false, nullable=true)
	private Boolean arhiviranKomentar;

	@ManyToOne
	@JoinColumn(name="kupac_fk")
	private Kupac kupac;
	
	@OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.EAGER, mappedBy="porudzbina")
	private Set<StavkaPorudzbine> stavkaPorudzbine = new HashSet<StavkaPorudzbine>();
	
	public Porudzbina() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	

	public Date getPorudzbinaDatum() {
		return porudzbinaDatum;
	}

	public void setPorudzbinaDatum(Date porudzbinaDatum) {
		this.porudzbinaDatum = porudzbinaDatum;
	}

	public Boolean getDostavljeno() {
		return dostavljeno;
	}

	public void setDostavljeno(Boolean dostavljeno) {
		this.dostavljeno = dostavljeno;
	}

	public Integer getOcena() {
		return ocena;
	}

	public void setOcena(Integer ocena) {
		this.ocena = ocena;
	}

	public String getKomentar() {
		return komentar;
	}

	public void setKomentar(String komentar) {
		this.komentar = komentar;
	}

	public Boolean getAnonimanKomentar() {
		return anonimanKomentar;
	}

	public void setAnonimanKomentar(Boolean anonimanKomentar) {
		this.anonimanKomentar = anonimanKomentar;
	}

	public Boolean getArhiviranKomentar() {
		return arhiviranKomentar;
	}

	public void setArhiviranKomentar(Boolean arhiviranKomentar) {
		this.arhiviranKomentar = arhiviranKomentar;
	}

	public Kupac getKupac() {
		return kupac;
	}

	public void setKupac(Kupac kupac) {
		this.kupac = kupac;
	}

	public Set<StavkaPorudzbine> getStavkaPorudzbine() {
		return stavkaPorudzbine;
	}

	public void setStavkaPorudzbine(Set<StavkaPorudzbine> stavkaPorudzbine) {
		this.stavkaPorudzbine = stavkaPorudzbine;
	}
	
}
