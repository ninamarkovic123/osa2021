package osa.api.model;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@DiscriminatorColumn(name="tip_korisnika")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Korisnik {

	public enum UlogaKorisnika {
		ROLE_ADMIN, ROLE_PRODAVAC, ROLE_KUPAC
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="korisnik_id", unique=true, nullable=false)
	private Integer id;
	
	@Column(name="ime", unique=false, nullable=false)
	private String ime;
	
	@Column(name="prezime", unique=false, nullable=false)
	private String prezime;
	
	@Column(name="korisnickoime", unique=false, nullable=false)
	private String korisnickoime;
	
	@Column(name="lozinka", unique=false, nullable=false)
	private String lozinka;
	
	@Column(name="blocked", unique=false, nullable=false)
	private Boolean blocked;
	
	@Column(name="uloga", unique=false, nullable=false)
	@Enumerated(EnumType.ORDINAL)
	private UlogaKorisnika uloga;
	
	public Korisnik() {
		
	}

	public Korisnik(String ime, String prezime, String korisnickoime, String lozinka, Boolean blocked,
			UlogaKorisnika uloga) {
		super();
		this.ime = ime;
		this.prezime = prezime;
		this.korisnickoime = korisnickoime;
		this.lozinka = lozinka;
		this.blocked = blocked;
		this.uloga = uloga;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getKorisnickoime() {
		return korisnickoime;
	}

	public void setKorisnickoime(String korisnickoime) {
		this.korisnickoime = korisnickoime;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public Boolean getBlocked() {
		return blocked;
	}

	public void setBlocked(Boolean blocked) {
		this.blocked = blocked;
	}

	public UlogaKorisnika getUloga() {
		return uloga;
	}

	public void setUloga(UlogaKorisnika uloga) {
		this.uloga = uloga;
	}
	
	
	
}
