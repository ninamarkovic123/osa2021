package osa.api.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonFormat;


@Entity
@DiscriminatorValue("Prodavac")
public class Prodavac extends Korisnik {
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
	@Column(name="radi_od", unique=false, nullable=true)
	private Date radiOd;
	
	@Column(name="email", unique=false, nullable=true)
	private String email;

	@Column(name="adresa", unique=false, nullable=true)
	private String adresa;
	
	@Column(name="naziv", unique=false, nullable=true)
	private String naziv;
		
	@OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="prodavac")
	private Set<Popust> popust = new HashSet<Popust>();
	
	@OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="prodavac")
	private Set<Artikal> artikli = new HashSet<Artikal>();

	public Prodavac() {
		
	}



	public Date getRadiOd() {
		return radiOd;
	}



	public void setRadiOd(Date radiOd) {
		this.radiOd = radiOd;
	}



	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Set<Popust> getPopust() {
		return popust;
	}

	public void setPopust(Set<Popust> popust) {
		this.popust = popust;
	}

	public Set<Artikal> getArtikli() {
		return artikli;
	}

	public void setArtikli(Set<Artikal> artikli) {
		this.artikli = artikli;
	}
	
}
