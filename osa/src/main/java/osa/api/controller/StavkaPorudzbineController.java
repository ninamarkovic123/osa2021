package osa.api.controller;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import osa.api.dto.StavkaPorudzbineDTO;
import osa.api.model.StavkaPorudzbine;
import osa.api.service.StavkaPorudzbinaInterface;
@RestController
@RequestMapping(value="prodavica/stavkaPorudzbine")
public class StavkaPorudzbineController {
	@Autowired
	private StavkaPorudzbinaInterface service;
	
	@GetMapping
	public ResponseEntity<List<StavkaPorudzbineDTO>> getAll(){
		List<StavkaPorudzbine> porudzbina = service.findAll();
		List<StavkaPorudzbineDTO> porudzbineDTO = new ArrayList<StavkaPorudzbineDTO>();
		for (StavkaPorudzbine o : porudzbina) {
			porudzbineDTO.add(new StavkaPorudzbineDTO(o));
		}
		return new ResponseEntity<List<StavkaPorudzbineDTO>>(porudzbineDTO, HttpStatus.OK);
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<StavkaPorudzbineDTO> getArtikal(@PathVariable("id") Integer id){
		StavkaPorudzbine o = service.findOne(id);
		if(o == null) {
			return new ResponseEntity<StavkaPorudzbineDTO>(HttpStatus.NOT_FOUND);
		}
		StavkaPorudzbineDTO aDTO = new StavkaPorudzbineDTO(o);
		return new ResponseEntity<StavkaPorudzbineDTO>(aDTO, HttpStatus.OK);
	}
	
	@PostMapping(consumes="application/json")
	public ResponseEntity<StavkaPorudzbineDTO> save(@RequestBody StavkaPorudzbineDTO aDTO){
		StavkaPorudzbine o = new StavkaPorudzbine();

		service.save(o);
		return new ResponseEntity<StavkaPorudzbineDTO>(aDTO, HttpStatus.CREATED);
	}
	
	@PutMapping(value="/{id}",consumes="application/json")
	public ResponseEntity<StavkaPorudzbineDTO> update(@RequestBody StavkaPorudzbineDTO aDTO, @PathVariable("id") Integer id){
		StavkaPorudzbine o = service.findOne(id);
		if(o == null) { return new ResponseEntity<StavkaPorudzbineDTO>(HttpStatus.NOT_FOUND); }
		
		service.save(o);
		StavkaPorudzbineDTO oDTO = new StavkaPorudzbineDTO(o);
		return new ResponseEntity<StavkaPorudzbineDTO>(oDTO, HttpStatus.OK);
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
		StavkaPorudzbine o = service.findOne(id);
		if (o != null) { service.remove(id); return new ResponseEntity<Void>(HttpStatus.OK);
		}
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}	

}
