package osa.api.controller;

import org.springframework.web.bind.annotation.RestController;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


import osa.api.dto.ArtikalDTO;
import osa.api.model.Artikal;
import osa.api.service.ArtikalServiceInterface;
import osa.api.service.KorisnikServiceInterface;

@RestController
@RequestMapping(value="prodavnica/artikli")
public class ArtikalController {

	@Autowired
	private ArtikalServiceInterface service;
	
	@Autowired
	private KorisnikServiceInterface korisnikService;
	
	@GetMapping(value="/prodavac/{naziv}")
	public ResponseEntity<List<ArtikalDTO>> getArtikliByProdavac(@PathVariable("naziv") String naziv){
		List<Artikal> artikli = service.findAllByProdavac(naziv);
		List<ArtikalDTO> artikliDTO = new ArrayList<ArtikalDTO>();
		for (Artikal a : artikli) {
			artikliDTO.add(new ArtikalDTO(a));
		}
		return new ResponseEntity<List<ArtikalDTO>>(artikliDTO, HttpStatus.OK);
	}
	
	@GetMapping(value="/for-prodavac/{id}")
	public ResponseEntity<List<ArtikalDTO>> getArtikliForProdavac(@PathVariable("id") Integer id){
		List<Artikal> artikli = service.findAllByProdvac_Id(id);
		List<ArtikalDTO> artikliDTO = new ArrayList<ArtikalDTO>();
		for (Artikal a : artikli) {
			artikliDTO.add(new ArtikalDTO(a));
		}
		return new ResponseEntity<List<ArtikalDTO>>(artikliDTO, HttpStatus.OK);
	}
	
	@GetMapping(value="/without-popust/{id}/{datum_od}/{datum_do}")
	public ResponseEntity<List<ArtikalDTO>> getArtikliWithoutPopust(@PathVariable("id") Integer id,@PathVariable("datum_od") String datum_od, @PathVariable("datum_do") String datum_do) throws ParseException{
		Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(datum_od);  
		Date date2=new SimpleDateFormat("yyyy-MM-dd").parse(datum_do);  
		List<Artikal> artikli = service.findAllByProdavacAndPopust(id, date1, date2);
		List<ArtikalDTO> artikliDTO = new ArrayList<ArtikalDTO>();
		for (Artikal a : artikli) {
			artikliDTO.add(new ArtikalDTO(a));
		}
		return new ResponseEntity<List<ArtikalDTO>>(artikliDTO, HttpStatus.OK);
	}
	
	@GetMapping(value="/admin")
	public ResponseEntity<List<ArtikalDTO>> getArtikli(){
		List<Artikal> artikli = service.findAll();
		List<ArtikalDTO> artikliDTO = new ArrayList<ArtikalDTO>();
		for (Artikal a : artikli) {
			artikliDTO.add(new ArtikalDTO(a));
		}
		return new ResponseEntity<List<ArtikalDTO>>(artikliDTO, HttpStatus.OK);
	}

	@GetMapping(value="/{id}")
	public ResponseEntity<ArtikalDTO> getArtikal(@PathVariable("id") Integer id){
		Artikal a = service.findOne(id);
		if(a == null) {
			return new ResponseEntity<ArtikalDTO>(HttpStatus.NOT_FOUND);
		}
		ArtikalDTO aDTO = new ArtikalDTO(a);
		return new ResponseEntity<ArtikalDTO>(aDTO, HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_PRODAVAC')")
	@PostMapping(consumes="application/json")
	public ResponseEntity<Boolean> save(@RequestBody ArtikalDTO aDTO) throws IOException{
		Artikal a = new Artikal();
		a.setNaziv(aDTO.getNaziv());
		a.setOpis(aDTO.getOpis());
		a.setCena(aDTO.getCena());
		a.setProdavac(korisnikService.findByKorisnickoime(aDTO.getKorisnickoimeProdavca()));
		a.setPath(aDTO.getNaziv()+".jpg" );
		
		service.save(a);
       // Path path = Paths.get("C:\\Users\\Nina\\git\\OSA2021\\osa\\src\\main\\resources\\static\\images\\"+ aDTO.getName()+".jpg");
        //Files.write(path, aDTO.getImgBytes()); 
		return new ResponseEntity<Boolean>(true, HttpStatus.CREATED);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_PRODAVAC')")
	@PutMapping(value="/{id}",consumes="application/json")
	public ResponseEntity<ArtikalDTO> update(@RequestBody ArtikalDTO aDTO, @PathVariable("id") Integer id){
		Artikal a = service.findOne(id);
		if(a == null) {return new ResponseEntity<ArtikalDTO>(HttpStatus.BAD_REQUEST);}
		a.setNaziv(aDTO.getNaziv());
		a.setOpis(aDTO.getOpis());
		a.setCena(aDTO.getCena());

		service.save(a);
		return new ResponseEntity<ArtikalDTO>(aDTO, HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_PRODAVAC')")
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
		if (service.findOne(id) != null) { service.remove(id); return new ResponseEntity<Void>(HttpStatus.OK); }
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
}
