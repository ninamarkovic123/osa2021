package osa.api.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import osa.api.dto.ArtikalDTO;
import osa.api.dto.PorudzbinaDTO;
import osa.api.dto.StavkaPorudzbineDTO;
import osa.api.model.Kupac;
import osa.api.model.Porudzbina;
import osa.api.model.StavkaPorudzbine;
import osa.api.service.ArtikalServiceInterface;
import osa.api.service.KorisnikServiceInterface;
import osa.api.service.PorudzbinaServiceInterface;
import osa.api.service.StavkaPorudzbinaInterface;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@RequestMapping(value="prodavnica/porudzbine")
public class PorudzbinaController {
	@Autowired
	private PorudzbinaServiceInterface service;
	
	@Autowired
	private StavkaPorudzbinaInterface stavkaPorudzbineService;
	
	@Autowired
	private ArtikalServiceInterface artikalService;
	
	@Autowired
	private KorisnikServiceInterface korisnikService;

	
	@GetMapping
	public ResponseEntity<List<PorudzbinaDTO>> getAll(){
		List<Porudzbina> porudzbine = service.findAll();
		List<PorudzbinaDTO> porudzbineDTO = new ArrayList<PorudzbinaDTO>();
		for (Porudzbina d : porudzbine) {
			porudzbineDTO.add(new PorudzbinaDTO(d));
		}
		return new ResponseEntity<List<PorudzbinaDTO>>(porudzbineDTO, HttpStatus.OK);
	}
	
	@GetMapping(value="/komentari")
	public ResponseEntity<List<PorudzbinaDTO>> getPorudzbineSaKomentarima(){
		List<Porudzbina> porudzbina = service.findByKomentarNotNullAndArhiviranKomentar(false);
		List<PorudzbinaDTO> porudzbineDTO = new ArrayList<PorudzbinaDTO>();
		for (Porudzbina d : porudzbina) {
			PorudzbinaDTO porudzbinaDTO = new PorudzbinaDTO(d);
			porudzbinaDTO.setKupac_korisnickoime(d.getKupac().getKorisnickoime());
			
			porudzbineDTO.add(porudzbinaDTO);
		}
		return new ResponseEntity<List<PorudzbinaDTO>>(porudzbineDTO, HttpStatus.OK);
	}
	
	@GetMapping(value="/kupac/{id}")
	public ResponseEntity<List<PorudzbinaDTO>> getPorudzbineZaKupac(@PathVariable("id") Integer id){
		Kupac c = korisnikService.getKupac(id);
		List<Porudzbina> porudzbina = service.findByKupac(c);
		List<PorudzbinaDTO> porudzbineDTO = new ArrayList<PorudzbinaDTO>();
		for (Porudzbina d : porudzbina) {
			PorudzbinaDTO o = new PorudzbinaDTO();
			o.setDatumPorudzbine(d.getPorudzbinaDatum());
			o.setId(d.getId());
			o.setDostavljeno(d.getDostavljeno());
			List<StavkaPorudzbineDTO> stavkaDTO = new ArrayList<StavkaPorudzbineDTO>();
			for(StavkaPorudzbine stavka : d.getStavkaPorudzbine()) {
				StavkaPorudzbineDTO oi = new StavkaPorudzbineDTO();
				oi.setArtikal(new ArtikalDTO(stavka.getArtikal()));
				oi.setId(stavka.getId());
				oi.setKolicina(stavka.getKolicina());
				oi.setPorudzbina(null);
				stavkaDTO.add(oi);
			}
			o.setStavka_porudzbine(stavkaDTO);
			porudzbineDTO.add(o);
			
		}
		return new ResponseEntity<List<PorudzbinaDTO>>(porudzbineDTO, HttpStatus.OK);
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<PorudzbinaDTO> get(@PathVariable("id") Integer id){
		Porudzbina o = service.getOne(id);
		if(o == null) {return new ResponseEntity<PorudzbinaDTO>(HttpStatus.NOT_FOUND);}
		PorudzbinaDTO oDTO = new PorudzbinaDTO(o);
		return new ResponseEntity<PorudzbinaDTO>(oDTO, HttpStatus.OK);
	}
	
	@Transactional
	@PostMapping(value="/{id}", consumes="application/json")
	public ResponseEntity<PorudzbinaDTO> save(@RequestBody PorudzbinaDTO oDTO,@PathVariable("id") Integer id){
		Porudzbina o = new Porudzbina();
		Kupac c = korisnikService.getKupac(id);
		o.setKupac(c);
		o.setPorudzbinaDatum(new Date());
		o.setDostavljeno(false);
		o.setArhiviranKomentar(false);
		
		service.save(o);
		for(StavkaPorudzbineDTO stavkaPorudzbineDTO : oDTO.getStavka_porudzbine()) {
			StavkaPorudzbine stavkaPorudzbine = new StavkaPorudzbine();
			stavkaPorudzbine.setArtikal(artikalService.findOne(stavkaPorudzbineDTO.getArtikal().getId()));
			stavkaPorudzbine.setKolicina(stavkaPorudzbineDTO.getKolicina());
			stavkaPorudzbine.setPorudzbina(o);
			stavkaPorudzbineService.save(stavkaPorudzbine);
			
		}
		return new ResponseEntity<PorudzbinaDTO>(new PorudzbinaDTO(o), HttpStatus.CREATED);
	}
	
	@PutMapping(value="/{id}",consumes="application/json")
	public ResponseEntity<PorudzbinaDTO> update(@RequestBody PorudzbinaDTO orDTO, @PathVariable("id") Integer id){
		Porudzbina o = service.getOne(id);
		if(o == null) { return new ResponseEntity<PorudzbinaDTO>(HttpStatus.NOT_FOUND);}
		PorudzbinaDTO oDTO = new PorudzbinaDTO(o);
		service.save(o);
		return new ResponseEntity<PorudzbinaDTO>(oDTO, HttpStatus.OK);
	}
	
	@PutMapping(value="/komentar/{id}",consumes="application/json")
	public ResponseEntity<PorudzbinaDTO> commentOrder(@RequestBody PorudzbinaDTO orDTO, @PathVariable("id") Integer id){
		Porudzbina o = service.getOne(id);
		if(o == null) {return new ResponseEntity<PorudzbinaDTO>(HttpStatus.NOT_FOUND);}
		o.setAnonimanKomentar(orDTO.getAnonimanKomentar());
		o.setKomentar(orDTO.getKomentar());
		o.setOcena(orDTO.getOcena());
		o.setDostavljeno(true);
		
		service.save(o);
		return new ResponseEntity<PorudzbinaDTO>(new PorudzbinaDTO(o), HttpStatus.OK);
	}
	
	@GetMapping(value="/arhiviran-komentar/{id}")
	public ResponseEntity<PorudzbinaDTO> archiveComment(@PathVariable("id") Integer id){
		Porudzbina o = service.getOne(id);
		if(o == null) {return new ResponseEntity<PorudzbinaDTO>(HttpStatus.NOT_FOUND);}
		o.setArhiviranKomentar(true);
	
		service.save(o);
		return new ResponseEntity<PorudzbinaDTO>(new PorudzbinaDTO(o), HttpStatus.OK);
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
		Porudzbina o = service.getOne(id);
		if (o != null) {service.remove(id);return new ResponseEntity<Void>(HttpStatus.OK);}
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}	

}
