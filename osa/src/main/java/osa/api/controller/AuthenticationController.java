package osa.api.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import osa.api.dto.KorisnikDTO;
import osa.api.dto.KupacDTO;
import osa.api.dto.ProdavacDTO;
import osa.api.model.Korisnik;
import osa.api.security.TokenUtils;
import osa.api.service.KorisnikServiceInterface;

@RestController
@RequestMapping(value="/api")
public class AuthenticationController {
	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired 
	private KorisnikServiceInterface service;
	
	@Autowired
	TokenUtils tokenUtils;
	
	@PostMapping(value="/login", consumes="application/json", produces="application/json")
	public ResponseEntity<KorisnikDTO> login(HttpServletResponse response,@RequestBody KorisnikDTO korisnikDTO) {
		Korisnik u = service.findByKorisnickoimeAndBlocked(korisnikDTO.getKorisnickoime(), false);
		if(u==null) {
			return new ResponseEntity<KorisnikDTO>( HttpStatus.NOT_FOUND);
		}
        try {
			UsernamePasswordAuthenticationToken authenticate = new UsernamePasswordAuthenticationToken(
					korisnikDTO.getKorisnickoime(), korisnikDTO.getLozinka());
            Authentication authentication = authenticationManager.authenticate(authenticate);
            UserDetails details = userDetailsService.loadUserByUsername(korisnikDTO.getKorisnickoime());
    	    response.addHeader("X-Auth-Token", tokenUtils.generateToken(details));
    	    
    	    KorisnikDTO kDTO = new KorisnikDTO(u.getId(),u.getIme(),u.getPrezime(),u.getKorisnickoime(),null,u.getBlocked(),u.getUloga().name());
    	    
            return new ResponseEntity<KorisnikDTO>(kDTO, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<KorisnikDTO>( HttpStatus.BAD_REQUEST);
        }
	}
	
	@PostMapping(value="/register/kupac", consumes="application/json")
	public ResponseEntity<Boolean> save(@RequestBody KupacDTO c){
		service.saveKupac(c);
		return new ResponseEntity<Boolean>(true, HttpStatus.CREATED);
	}
	
	@PostMapping(value="/register/prodavac", consumes="application/json")
	public ResponseEntity<Boolean> save(@RequestBody ProdavacDTO s){
		service.saveProdavac(s);
		return new ResponseEntity<Boolean>(true, HttpStatus.CREATED);
	}
}
