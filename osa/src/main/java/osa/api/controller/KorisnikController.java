package osa.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import osa.api.dto.KorisnikDTO;
import osa.api.dto.ProdavacDTO;
import osa.api.model.Korisnik;
import osa.api.model.Prodavac;
import osa.api.service.KorisnikServiceInterface;

@RestController
@RequestMapping(value="prodavnica/korisnici")
public class KorisnikController {

	@Autowired
	private KorisnikServiceInterface service;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	
	@GetMapping
	public ResponseEntity<List<KorisnikDTO>> getAll(){
		List<Korisnik> korisnici = service.findAll();
		List<KorisnikDTO> korisnikDTO = new ArrayList<KorisnikDTO>();
		for (Korisnik u : korisnici) {
			u.setLozinka(null);
			korisnikDTO.add(new KorisnikDTO(u));
			
		}
		return new ResponseEntity<List<KorisnikDTO>>(korisnikDTO, HttpStatus.OK);
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<KorisnikDTO> getOne(@PathVariable("id") Integer id){
		Korisnik u = service.getOne(id);
		if(u == null) {
			return new ResponseEntity<KorisnikDTO>(HttpStatus.NOT_FOUND);
		}
		KorisnikDTO uDTO = new KorisnikDTO(u);
		return new ResponseEntity<KorisnikDTO>(uDTO, HttpStatus.OK);
	}	
	
	
	@GetMapping(value="/prodavac")
	public ResponseEntity<List<ProdavacDTO>> getSalesmans(){
		List<Prodavac> prodavci = service.getAllProdavci();
		List<ProdavacDTO> prodavacDTO = new ArrayList<ProdavacDTO>();
		for (Prodavac s : prodavci) {
			s.setLozinka(null);
			
			prodavacDTO.add(new ProdavacDTO(s));
		}
		return new ResponseEntity<List<ProdavacDTO>>(prodavacDTO, HttpStatus.OK);
	}	
	
	
	//svaki korisnik
	@PutMapping(value="/{id}",consumes="application/json")
	public ResponseEntity<KorisnikDTO> update(@RequestBody KorisnikDTO usDTO, @PathVariable("id") Integer id){
		Korisnik u = service.getOne(id);
		if(u == null) {
			return new ResponseEntity<KorisnikDTO>(HttpStatus.NOT_FOUND);
		}
		u.setIme(usDTO.getIme());
		u.setPrezime(usDTO.getPrezime());
		
		service.saveKorisnik(u);
		KorisnikDTO uDTO = new KorisnikDTO(u);
		return new ResponseEntity<KorisnikDTO>(uDTO, HttpStatus.OK);
	}
	
	//samo admin 
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
		Korisnik u = service.getOne(id);
		if (u != null) {
			service.remove(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
		}
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
	
	//samo admin
	@PutMapping(value="/status/{korisnickoime}", consumes="application/json")
	public ResponseEntity<Boolean> changeBlockStatus(@PathVariable("korisnickoime") String korisnickoime,@RequestBody Boolean block){
		Korisnik u = service.findByKorisnickoime(korisnickoime);
		if(u == null) {
			return new ResponseEntity<Boolean>(false, HttpStatus.NOT_FOUND);
		}
		u.setBlocked(block);
		service.editKorisnik(u);
		return new ResponseEntity<Boolean>(true, HttpStatus.CREATED);
	}
	
	//svaki korisnik
		@PutMapping(value="/izmeni-lozinku/{korisnickoime}", consumes="application/json")
		public ResponseEntity<Boolean> izmeniLozinku(@PathVariable("korisnickoime") String korisnickoime,@RequestBody KorisnikDTO uDTO){
			Korisnik u = service.findByKorisnickoime(uDTO.getKorisnickoime());
			if(u == null) {
				return new ResponseEntity<Boolean>(false, HttpStatus.NOT_FOUND);
			}
			System.out.println("sent old password: "+ uDTO.getLozinka());

			u.setLozinka(passwordEncoder.encode(uDTO.getNova_lozinka()));
		
			service.changePassword(u);
			return new ResponseEntity<Boolean>(true, HttpStatus.CREATED);
		}
	
}
