package osa.api.controller;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import osa.api.dto.ArtikalDTO;
import osa.api.dto.PopustDTO;
import osa.api.model.Artikal;
import osa.api.model.Popust;
import osa.api.model.Prodavac;
import osa.api.service.ArtikalServiceInterface;
import osa.api.service.KorisnikServiceInterface;
import osa.api.service.PopustServiceInterface;



@RestController
@RequestMapping(value="prodavnica/popusti")
public class PopustController {
	@Autowired
	private PopustServiceInterface service;
	
	@Autowired
	private KorisnikServiceInterface korisnikService;
	
	@Autowired
	private ArtikalServiceInterface artikliService;
	
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping
	public ResponseEntity<List<PopustDTO>> getPopust(){
		List<Popust> popusti = service.findAll();
		List<PopustDTO> popustiDTO = new ArrayList<PopustDTO>();
		for (Popust p : popusti) {
			popustiDTO.add(new PopustDTO(p));
		}
		return new ResponseEntity<List<PopustDTO>>(popustiDTO, HttpStatus.OK);
	}
	
	@GetMapping(value="/prodavac/{id}")
	public ResponseEntity<List<PopustDTO>> getPoputForProdavac(@PathVariable("id") Integer id){
		Prodavac s = korisnikService.getProdavac(id);
		List<Popust> popusti = service.findByProdavac(s);
		List<PopustDTO> popustDTO = new ArrayList<PopustDTO>();
		for (Popust d : popusti) {
			popustDTO.add(new PopustDTO(d));
		}
		return new ResponseEntity<List<PopustDTO>>(popustDTO, HttpStatus.OK);
	}
	
	
	@GetMapping(value="/{id}")
	public ResponseEntity<PopustDTO> getPopust(@PathVariable("id") Integer id){
		Popust d = service.findOne(id);
		if(d == null) {return new ResponseEntity<PopustDTO>(HttpStatus.NOT_FOUND); }
		PopustDTO popustDTO = new PopustDTO(d);
		return new ResponseEntity<PopustDTO>(popustDTO, HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_PRODAVAC')")
	@PostMapping(value="/{id}", consumes="application/json")
	public ResponseEntity<PopustDTO> napraviNoviPopust (@RequestBody PopustDTO pDTO, @PathVariable("id") Integer id){
		Popust p = new Popust();
		p.setProcenat(pDTO.getProcenat());
		p.setDatumOd(pDTO.getDatumOd());
		p.setDatumDo(pDTO.getDatumDo());
		p.setText(pDTO.getText());
		p.setProdavac(korisnikService.getProdavac(id));
		Set<Artikal> artikli = new HashSet<Artikal>();
		for(ArtikalDTO a: pDTO.getArtikli()) {
			artikli.add(artikliService.findOne(a.getId())); }
		p.setArtikli(artikli);
		service.save(p);
		return new ResponseEntity<PopustDTO>(pDTO, HttpStatus.CREATED);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_KUPAC')")
	@PutMapping(value="/{id}",consumes="application/json")
	public ResponseEntity<PopustDTO> save(@RequestBody PopustDTO dDTO, @PathVariable("id") Integer id){
		Popust d = service.findOne(id);
		if (d == null) { return new ResponseEntity<PopustDTO>(HttpStatus.BAD_REQUEST);}
		//set
		service.save(d);
		PopustDTO popustDTO = new PopustDTO(d);
		return new ResponseEntity<PopustDTO>(popustDTO, HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_PRODAVAC')")
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
		Popust d = service.findOne(id);
		System.out.println(id);
		if (d != null) { service.remove(id); return new ResponseEntity<Void>(HttpStatus.OK); }
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
}
